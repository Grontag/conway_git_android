package com.keekorok.conway.conway.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.keekorok.conway.conway.Beans.Articulo;
import com.keekorok.conway.conway.R;

import java.util.ArrayList;

/**
 * Created by mac-27 on 16/3/17.
 */

public class DetallesPedidosAdapter extends RecyclerView.Adapter<DetallesPedidosAdapter.ArticulosViewHolder> implements View.OnClickListener{

    private ArrayList<Articulo> datos;
    private View.OnClickListener listener;


    public DetallesPedidosAdapter(ArrayList<Articulo> datos) {
        this.datos = datos;
    }

    @Override
    public ArticulosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detallepedidoceldalayout, parent, false);

        itemView.setOnClickListener(this);

        ArticulosViewHolder avh=new ArticulosViewHolder(itemView);

        return avh;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(ArticulosViewHolder holder, int position) {
        Articulo articulo=datos.get(position);
        holder.bindArticulo(articulo);
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onClick(v);
    }

    public static class ArticulosViewHolder extends RecyclerView.ViewHolder{

        private TextView descripcion;
        private TextView pack;
        private TextView precioUnidad;
        private TextView importe;
        private TextView unidades;
        private Context ctx;


        public ArticulosViewHolder(View itemView) {
            super(itemView);
            descripcion=(TextView)itemView.findViewById(R.id.desc);
            pack=(TextView)itemView.findViewById(R.id.packtv);
            precioUnidad=(TextView)itemView.findViewById(R.id.punidadtv);
            importe=(TextView)itemView.findViewById(R.id.imptv);
            unidades=(TextView)itemView.findViewById(R.id.unitv);
            ctx=itemView.getContext();
            Typeface tFaceUltralight=Typeface.createFromAsset(ctx.getAssets(),"fonts/HelveticaNeueUltraLight.ttf");
            Typeface tFaceLight=Typeface.createFromAsset(ctx.getAssets(), "fonts/HelveticaNeueLight.ttf");
            descripcion.setTypeface(tFaceLight);
            importe.setTypeface(tFaceLight);
            pack.setTypeface(tFaceUltralight);
            precioUnidad.setTypeface(tFaceUltralight);
            unidades.setTypeface(tFaceUltralight);

        }

        public void bindArticulo(Articulo articulo){

            descripcion.setText(articulo.getDescripcion());
            pack.setText(String.valueOf(articulo.getPack()));
            precioUnidad.setText(String.valueOf(articulo.getPrecio()));
            importe.setText(String.valueOf(articulo.getImporteTotal()));
            unidades.setText(String.valueOf(articulo.getUnidades()));
        }

    }
}

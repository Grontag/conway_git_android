package com.keekorok.conway.conway;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.keekorok.conway.conway.Activities.DetallePedidoActivity;
import com.keekorok.conway.conway.Activities.ListActivity;
import com.keekorok.conway.conway.Beans.LoginResponse;
import com.keekorok.conway.conway.Interfaces.RestClient;
import com.keekorok.conway.conway.Services.ApiCliente;
import com.keekorok.conway.conway.Services.Services;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {

    private TextInputLayout tilLogin;
    private TextInputLayout tilPass;
    private TextView loginButton;
    private TextView lostPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tilLogin=(TextInputLayout)findViewById(R.id.tillogin);
        Typeface tFaceLight=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLight.ttf");
        Typeface tFaceUltralight=Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeueUltraLight.ttf");
        tilPass=(TextInputLayout)findViewById(R.id.tilpass);
        tilLogin.setTypeface(tFaceLight);
        tilPass.setTypeface(tFaceLight);
        loginButton=(TextView) findViewById(R.id.tventrar);
        loginButton.setTypeface(tFaceLight);
        lostPass=(TextView)findViewById(R.id.tvlostpass);
        lostPass.setTypeface(tFaceLight);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(Services.doLogin(tilLogin.getEditText().getText().toString(),tilPass.getEditText().getText().toString())){
                    goToListActivity();
                }else{
                    showAlertDialog();
                }*/
                RestClient serviceLogin = ApiCliente.getService().create(RestClient.class);
                Call<String> loginResponse = serviceLogin.login(tilLogin.getEditText().getText().toString(), tilPass.getEditText().getText().toString());
                loginResponse.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if(response.isSuccessful()){
                            //La respuesta ha sido exitosa. Vamos a la Activity de la lista
                            goToListActivity();
                        }else{
                            //La respuesta no es exitosa. Mostramos un diálogo para informar al usuario.
                            showAlertDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.i("David", "onFailure");
                        t.printStackTrace();
                    }
                });
            }
        });

        lostPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Aquí el código para la recuperación de la contraseña, mediante un servicio.
                final Dialog customDialog=new Dialog(LoginActivity.this);
                customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                customDialog.setCancelable(false);
                customDialog.setContentView(R.layout.retrievepasslayout);
                Button getPass=(Button)customDialog.findViewById(R.id.button);
                Button cancel=(Button)customDialog.findViewById(R.id.button2);
                final TextInputLayout emailTIL=(TextInputLayout)customDialog.findViewById(R.id.textInputLayout);
                getPass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email=emailTIL.getEditText().getText().toString();
                        lanzarPeticion(email);
                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });
                customDialog.show();
            }
        });
    }

    private void lanzarPeticion(String email) {
        //Aquí enviaremos una petición para que nos envíe el pass al email, una vez que tengamos dicho servicio operativo.
    }

    private void goToListActivity() {
        Intent intent=new Intent(LoginActivity.this, ListActivity.class);
        intent.putExtra("username", tilLogin.getEditText().getText().toString());
        startActivity(intent);
    }

    private void showAlertDialog(){
        AlertDialog.Builder aDialog=new AlertDialog.Builder(LoginActivity.this);
        aDialog.setTitle(getString(R.string.error));
        aDialog.setMessage(getString(R.string.loginfailure));
        aDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                tilLogin.getEditText().setText("");
                tilPass.getEditText().setText("");
                tilLogin.requestFocus();
                //SÓLO PARA PRUEBAS
                //Aunque falle, dado que no tenemos servicios aún, dejamos pasar.
                goToListActivity();
            }
        });
        aDialog.show();
    }
}

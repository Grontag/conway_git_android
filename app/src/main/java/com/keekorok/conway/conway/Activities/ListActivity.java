package com.keekorok.conway.conway.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.keekorok.conway.conway.Adapters.RecyclerViewAdapter;
import com.keekorok.conway.conway.Beans.Pedido;
import com.keekorok.conway.conway.Interfaces.RestClient;
import com.keekorok.conway.conway.R;
import com.keekorok.conway.conway.Services.ApiCliente;
import com.keekorok.conway.conway.Services.Services;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mac-27 on 14/3/17.
 */

public class ListActivity extends AppCompatActivity {

    private ImageButton backButton;
    private ImageButton addButton;
    private RecyclerView rView;
    public static ArrayList<Pedido> datos;
    private RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listactivitylayout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        backButton=(ImageButton)findViewById(R.id.backbutton);
        addButton=(ImageButton)findViewById(R.id.addButton);
        rView=(RecyclerView)findViewById(R.id.rView);
        rView.setHasFixedSize(true);
        if(datos==null){
            datos=new ArrayList<>();
        }

        datos=Services.getPedidos();
        /*RestClient serviceLogin = ApiCliente.getService().create(RestClient.class);
        Call<Pedido> peticion=serviceLogin.getPedidos();
        peticion.enqueue(new Callback<Pedido>() {
            @Override
            public void onResponse(Call<Pedido> call, Response<Pedido> response) {
                if(response.isSuccessful()){
                    //Aquí rellenaríamos el ArrayList datos con los datos recibidos desde el servicio web con los pedidos.
                    Log.i("David", "onResponse es succesful");
                }else{
                    //La petición no ha tenido éxito, mostraríamos un mensaje al usuario.
                    Log.i("David", "onResponse no es succesful");
                    Log.i("David", "Para hacer pruebas, ponemos pedidos a pelo");
                    datos=Services.getPedidos();
                    if(adapter==null){
                        adapter=new RecyclerViewAdapter(datos);
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Pedido> call, Throwable t) {
                Log.i("David", "onFailure pidiendo los pedidos");

            }
        });*/

        adapter=new RecyclerViewAdapter(datos);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Aquí tenemos dos opciones, o bien pasamos los datos del pedido a pelo en el intent, o pedimos a un servicio web que nos devuelva los datos, en base a su número de pedido.
                Log.i("David", "Hemos pulsado el elemento "+rView.getChildLayoutPosition(v));
                Log.i("David", "En el elemento seleccionado, la fecha es: "+datos.get(rView.getChildLayoutPosition(v)).getDia()+ " "+datos.get(rView.getChildLayoutPosition(v)).getMes());
                Intent intent=new Intent(ListActivity.this, DetallePedidoActivity.class);
                intent.putExtra("numPedido", datos.get(rView.getChildLayoutPosition(v)).getnPedido());
                intent.putExtra("mes", datos.get(rView.getChildLayoutPosition(v)).getMes());
                intent.putExtra("dia", datos.get(rView.getChildLayoutPosition(v)).getDia());
                intent.putExtra("año", datos.get(rView.getChildLayoutPosition(v)).getAño());
                intent.putExtra("importe", datos.get(rView.getChildLayoutPosition(v)).getImporte());
                intent.putExtra("peso", datos.get(rView.getChildLayoutPosition(v)).getPeso());
                intent.putExtra("abierto",datos.get(rView.getChildLayoutPosition(v)).isAbierto());
                startActivity(intent);
            }
        });
        rView.setAdapter(adapter);
        rView.setLayoutManager(
                new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter!=null){
            adapter.notifyDataSetChanged();
        }
    }
}

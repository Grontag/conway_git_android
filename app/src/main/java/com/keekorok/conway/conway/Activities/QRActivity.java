package com.keekorok.conway.conway.Activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.Result;
import com.keekorok.conway.conway.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by mac-27 on 12/4/17.
 */

public class QRActivity extends Activity implements ZXingScannerView.ResultHandler{

    private ZXingScannerView mScannerView;
    private TextView resultadoQr;
    private TextView encabezado;
    private ViewGroup scanArea;
    private Button addButton;
    private Button flashButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(R.layout.qrlayout);
        Typeface tFaceLight=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLight.ttf");
        Typeface tFaceUltralight=Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeueUltraLight.ttf");
        scanArea=(ViewGroup)findViewById(R.id.frameLayout);
        encabezado=(TextView)findViewById(R.id.textView3);
        encabezado.setVisibility(View.GONE);
        encabezado.setTypeface(tFaceLight);
        resultadoQr=(TextView)findViewById(R.id.textView5);
        resultadoQr.setText("");
        resultadoQr.setTypeface(tFaceUltralight);
        addButton=(Button)findViewById(R.id.addButton);
        addButton.setVisibility(View.GONE);
        flashButton=(Button)findViewById(R.id.flashbutton);
        flashButton.setText(getString(R.string.flashoff));
        flashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mScannerView.getFlash()){
                    mScannerView.setFlash(false);
                    flashButton.setText(getString(R.string.flashoff));
                }else{
                    mScannerView.setFlash(true);
                    flashButton.setText(getString(R.string.flashon));
                }
            }
        });
        scanArea.addView(mScannerView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void handleResult(final Result result) {
        Log.i("David", "handleResult llamado. Resultado: "+result.getText());
        mScannerView.resumeCameraPreview(this);
        resultadoQr.setText(result.getText());
        addButton.setVisibility(View.VISIBLE);
        encabezado.setVisibility(View.VISIBLE);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String resultadoLectura=result.getText();
                //Aquí manejamos lo que queremos hacer con el resultado.
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.setFlash(false);
    }
}

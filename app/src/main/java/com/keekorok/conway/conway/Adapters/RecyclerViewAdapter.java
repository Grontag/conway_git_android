package com.keekorok.conway.conway.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.keekorok.conway.conway.Beans.Pedido;
import com.keekorok.conway.conway.R;

import java.util.ArrayList;

/**
 * Created by mac-27 on 14/3/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.PedidosViewHolder>  implements View.OnClickListener{

    private ArrayList<Pedido> datos;
    private View.OnClickListener listener;

    public RecyclerViewAdapter(ArrayList<Pedido> datos) {
        this.datos = datos;
    }

    @Override
    public PedidosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pedidolayout, parent, false);

        itemView.setOnClickListener(this);

        PedidosViewHolder pvh = new PedidosViewHolder(itemView);

        return pvh;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }

    @Override
    public void onBindViewHolder(PedidosViewHolder holder, int position) {

        Pedido pedido=datos.get(position);
        holder.bindPedido(pedido);
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }



    public static class PedidosViewHolder extends RecyclerView.ViewHolder{

        private TextView dia;
        //private TextView fecha;
        private ImageView estado;
        private TextView mes;
        private TextView year;
        private TextView nPedido;
        private Context ctx;


        public PedidosViewHolder(View itemView) {
            super(itemView);
            dia=(TextView)itemView.findViewById(R.id.diatview);
            mes=(TextView)itemView.findViewById(R.id.mestview);
            year=(TextView)itemView.findViewById(R.id.yeartview);
            //fecha=(TextView)itemView.findViewById(R.id.fechatview);
            nPedido=(TextView)itemView.findViewById(R.id.npedidotview);
            estado=(ImageView)itemView.findViewById(R.id.estadoimageview);
            ctx=itemView.getContext();
            Typeface tFaceUltralight=Typeface.createFromAsset(ctx.getAssets(),"fonts/HelveticaNeueUltraLight.ttf");
            dia.setTypeface(tFaceUltralight);
            Typeface tFaceLight=Typeface.createFromAsset(ctx.getAssets(), "fonts/HelveticaNeueLight.ttf");
            //fecha.setTypeface(tFaceLight);
            mes.setTypeface(tFaceLight);
            year.setTypeface(tFaceLight);
            nPedido.setTypeface(tFaceLight);
        }

        public void bindPedido(Pedido pedido){
            dia.setText(pedido.getDia());
            //fecha.setText(pedido.getFecha());
            mes.setText(pedido.getMes());
            year.setText(pedido.getAño());
            nPedido.setText(pedido.getnPedido());
            if(pedido.isAbierto()){
                //estado.setImageResource(R.drawable.candadoabierto);
                estado.setVisibility(View.INVISIBLE);
                dia.setTextColor(ContextCompat.getColor(ctx, R.color.rojo));
                //fecha.setTextColor(ContextCompat.getColor(ctx, R.color.rojo));
                mes.setTextColor(ContextCompat.getColor(ctx, R.color.rojo));
                year.setTextColor(ContextCompat.getColor(ctx, R.color.rojo));
                nPedido.setTextColor(ContextCompat.getColor(ctx, R.color.rojo));
            }else{
                estado.setImageResource(R.drawable.candadocerrado);
                dia.setTextColor(ContextCompat.getColor(ctx, R.color.negro));
                //fecha.setTextColor(ContextCompat.getColor(ctx, R.color.negro));
                mes.setTextColor(ContextCompat.getColor(ctx, R.color.negro));
                year.setTextColor(ContextCompat.getColor(ctx, R.color.negro));
                nPedido.setTextColor(ContextCompat.getColor(ctx, R.color.negro));
            }
            //estado=pedido.isAbierto();
        }
    }
}

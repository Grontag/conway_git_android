package com.keekorok.conway.conway.Services;

import com.keekorok.conway.conway.Beans.Articulo;
import com.keekorok.conway.conway.Beans.Pedido;

import java.util.ArrayList;

/**
 * Created by mac-27 on 14/3/17.
 */

public class Services {

    private static ArrayList<Pedido> pedidos;
    public static boolean doLogin(String user, String pass){
        if(!user.equals("") && !pass.equals("")){
            return true;
        }else{
            return false;
        }
    }

    public static ArrayList<Pedido> getPedidos(){
        pedidos=new ArrayList<>();

        Pedido pedido=new Pedido();
        pedido.setDia("25");
        pedido.setMes("Marzo");
        pedido.setAño("2017 sáb.");
        pedido.setnPedido("1000");
        //pedido.setFecha("Marzo 2017");
        pedido.setAbierto(true);
        pedido.setImporte(20.5);
        pedido.setPeso(2.5F);
        pedidos.add(pedido);

        Pedido pedido2=new Pedido();
        pedido2.setDia("13");
        pedido2.setMes("Abril");
        pedido2.setAño("2017 jue.");
        pedido2.setnPedido("1001");
        //pedido2.setFecha("Abril 2017");
        pedido2.setAbierto(false);
        pedido2.setImporte(13.25);
        pedido2.setPeso(5.5F);
        pedidos.add(pedido2);

        Pedido pedido3=new Pedido();
        pedido3.setDia("3");
        pedido3.setMes("Febrero");
        pedido3.setAño("2017 vie.");
        pedido3.setnPedido("1002");
        //pedido3.setFecha("Febrero 2017");
        pedido3.setAbierto(false);
        pedido3.setImporte(30.40);
        pedido3.setPeso(15F);
        pedidos.add(pedido3);

        return pedidos;
    }

    public static ArrayList<Articulo> getArticuloFromPedido(String numPedido){

        ArrayList<Articulo> articulos=new ArrayList<>();

        //Aquí haríamos una petición a un servicio web que nos devolviera los artículos de un pedido concreto. Mientras se hace, devolveremos una lista fija.
        Articulo art1=new Articulo();
        art1.setDescripcion("Descripción art1");
        art1.setPrecio(6.2);
        art1.setPack(8);
        art1.setUnidades(3);
        art1.setImporteTotal(art1.getPrecio()*art1.getUnidades());
        articulos.add(art1);

        Articulo art2=new Articulo();
        art2.setDescripcion("Descripción art2");
        art2.setPrecio(5.5);
        art2.setPack(3);
        art2.setUnidades(12);
        art2.setImporteTotal(art2.getPrecio()*art2.getUnidades());
        articulos.add(art2);

        Articulo art3=new Articulo();
        art3.setDescripcion("Descripcion art3");
        art3.setPrecio(4.25);
        art3.setPack(2);
        art3.setUnidades(7);
        art3.setImporteTotal(art3.getPrecio()*art3.getUnidades());
        articulos.add(art3);

        return articulos;
    }
}

package com.keekorok.conway.conway.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.keekorok.conway.conway.Adapters.DetallesPedidosAdapter;
import com.keekorok.conway.conway.Beans.Articulo;
import com.keekorok.conway.conway.Beans.Pedido;
import com.keekorok.conway.conway.Beans.SearchBean;
import com.keekorok.conway.conway.Interfaces.RestClient;
import com.keekorok.conway.conway.R;
import com.keekorok.conway.conway.Services.ApiCliente;
import com.keekorok.conway.conway.Services.Services;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mac-27 on 15/3/17.
 */

public class DetallePedidoActivity extends AppCompatActivity {

    private RecyclerView rView;
    private ImageButton backButton;
    private ImageButton searchButton;
    private ImageButton qrButton;
    private ImageButton duplicateButton;
    private ImageButton reloadButton;
    private TextView importeTotal;
    private TextView numPedidoTV;
    private String numPedido;
    private String mes;
    private String dia;
    private String año;
    private Double importe;
    private Float peso;
    private boolean isAbierto;
    private ArrayList<Articulo> articulos;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detallepedidolayout);
        Typeface tFaceLight=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLight.ttf");
        Typeface tFaceUltralight=Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeueUltraLight.ttf");
        numPedido=getIntent().getExtras().getString("numPedido");
        mes=getIntent().getExtras().getString("mes");
        dia=getIntent().getExtras().getString("dia");
        año=getIntent().getExtras().getString("año");
        importe=getIntent().getExtras().getDouble("importe");
        peso=getIntent().getExtras().getFloat("peso");
        isAbierto=getIntent().getExtras().getBoolean("abierto");
        articulos= Services.getArticuloFromPedido("xxx");
        if(articulos==null){
            articulos=new ArrayList<>();
        }

        /*RestClient serviceLogin = ApiCliente.getService().create(RestClient.class);
        Call<Articulo> peticion=serviceLogin.getArticulosForNumPedido(numPedido);
        peticion.enqueue(new Callback<Articulo>() {
            @Override
            public void onResponse(Call<Articulo> call, Response<Articulo> response) {
                if(response.isSuccessful()){
                    //Aquí llenaremos el arraylist de artículos devuelto por el servicio, si la petición es exitosa.
                }else{
                    //La petición web ha fallado, sacamos un diçalogo para informar al usuario de ello.
                }
            }

            @Override
            public void onFailure(Call<Articulo> call, Throwable t) {

            }
        });*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        backButton=(ImageButton)findViewById(R.id.backbutton);
        searchButton=(ImageButton)findViewById(R.id.searchbutton);
        qrButton=(ImageButton)findViewById(R.id.qrcodebutton);
        duplicateButton=(ImageButton)findViewById(R.id.duplicateButton);
        //numPedidoTV=(TextView)findViewById(R.id.npedidotview);
        numPedidoTV=(TextView)findViewById(R.id.tvnumpedido);
        numPedidoTV.setText(numPedido);
        numPedidoTV.setTypeface(tFaceLight);
        importeTotal=(TextView)findViewById(R.id.importetotalTV);
        calcularImporteTotal();
        rView=(RecyclerView)findViewById(R.id.recyclerView);
        rView.setHasFixedSize(true);
        final DetallesPedidosAdapter adapter=new DetallesPedidosAdapter(articulos);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("David", "Hemos pulsado el elemento "+rView.getChildLayoutPosition(v)+". La descripción del artículo: "+articulos.get(rView.getChildLayoutPosition(v)).getDescripcion());
                final Articulo articulo=articulos.get(rView.getChildLayoutPosition(v));
                final Dialog customDialog=new Dialog(DetallePedidoActivity.this);
                customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                customDialog.setCancelable(false);
                customDialog.setContentView(R.layout.detallearticulolayout);
                TextView codigo=(TextView)customDialog.findViewById(R.id.codigotv);
                TextView descripcion=(TextView)customDialog.findViewById(R.id.descarticulotv);
                TextView pack=(TextView)customDialog.findViewById(R.id.packtv);
                TextView precio=(TextView)customDialog.findViewById(R.id.preciotv);
                final EditText cantidad=(EditText)customDialog.findViewById(R.id.cantidadet);
                cantidad.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                cantidad.setRawInputType(InputType.TYPE_CLASS_NUMBER);
                TextView cancel=(TextView)customDialog.findViewById(R.id.cancelartv);
                TextView ok=(TextView)customDialog.findViewById(R.id.oktv);
                codigo.setText(articulo.getCodigo());
                descripcion.setText(articulo.getDescripcion());
                pack.setText(String.valueOf(articulo.getPack()));
                precio.setText(String.valueOf(articulo.getPrecio()));
                cantidad.setText(String.valueOf(articulo.getUnidades()));
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        articulo.setUnidades(Integer.parseInt(cantidad.getText().toString()));
                        articulo.setImporteTotal(articulo.getPrecio()*articulo.getUnidades());
                        calcularImporteTotal();
                        adapter.notifyDataSetChanged();
                        customDialog.dismiss();
                    }
                });
                customDialog.show();
            }
        });
        rView.setAdapter(adapter);
        rView.setLayoutManager(
                new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog searchDialog=new Dialog(DetallePedidoActivity.this);
                searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                searchDialog.setCancelable(false);
                searchDialog.setContentView(R.layout.busquedalayout);
                final EditText codigoEt=(EditText)searchDialog.findViewById(R.id.codigoet);
                codigoEt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(codigoEt.getText().toString().equals(getString(R.string.codigo))){
                            codigoEt.setText("");
                        }

                    }
                });
                codigoEt.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                if(codigoEt.getText().toString().equals(getString(R.string.codigo))){
                                    codigoEt.setText("");
                                    codigoEt.requestFocus();
                                }
                        }
                        return false;
                    }
                });
                codigoEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(!hasFocus){
                            if(codigoEt.getText().toString().equals("")){
                                codigoEt.setText(getString(R.string.codigo));
                            }
                        }
                    }
                });
                final EditText familiaEt=(EditText)searchDialog.findViewById(R.id.familiaet);
                familiaEt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(familiaEt.getText().toString().equals(getString(R.string.family))){
                            familiaEt.setText("");
                        }
                    }
                });

                familiaEt.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                if(familiaEt.getText().toString().equals(getString(R.string.family))){
                                    familiaEt.setText("");
                                    familiaEt.requestFocus();
                                }
                                break;
                        }
                        return false;
                    }
                });

                familiaEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(!hasFocus){
                            if(familiaEt.getText().toString().equals("")){
                                familiaEt.setText(getString(R.string.family));
                            }
                        }
                    }
                });

                final EditText referenciaEt=(EditText)searchDialog.findViewById(R.id.referenciaet);
                referenciaEt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(referenciaEt.getText().toString().equals(getString(R.string.referencia))){
                            referenciaEt.setText("");
                        }

                    }
                });

                referenciaEt.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                if(referenciaEt.getText().toString().equals(getString(R.string.referencia))){
                                    referenciaEt.setText("");
                                    referenciaEt.requestFocus();
                                }
                        }
                        return false;
                    }
                });
                referenciaEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(!hasFocus){
                            if(referenciaEt.getText().toString().equals("")){
                                referenciaEt.setText(getString(R.string.referencia));
                            }
                        }
                    }
                });
                final EditText descripcionEt=(EditText)searchDialog.findViewById(R.id.descripcionet);
                descripcionEt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(descripcionEt.getText().toString().equals(getString(R.string.descripcion))){
                            descripcionEt.setText("");
                        }

                    }
                });

                descripcionEt.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()){
                            case MotionEvent.ACTION_DOWN:
                                if(descripcionEt.getText().toString().equals(getString(R.string.descripcion))){
                                    descripcionEt.setText("");
                                    descripcionEt.requestFocus();
                                }
                        }
                        return false;
                    }
                });
                descripcionEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(!hasFocus){
                            if(descripcionEt.getText().toString().equals("")){
                                descripcionEt.setText(getString(R.string.descripcion));
                            }
                        }
                    }
                });
                TextView ok=(TextView)searchDialog.findViewById(R.id.oktv);
                TextView cancelar=(TextView)searchDialog.findViewById(R.id.cancelartv);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String codigo="";
                        String familia="";
                        String referencia="";
                        String descripcion="";
                        if(!codigoEt.getText().toString().equals("") && !codigoEt.getText().toString().equals(getString(R.string.codigo))){
                            codigo=codigoEt.getText().toString();
                        }
                        if(!familiaEt.getText().toString().equals("") && !familiaEt.getText().toString().equals(getString(R.string.family))){
                            familia=familiaEt.getText().toString();
                        }
                        if(!referenciaEt.getText().toString().equals("") && !referenciaEt.getText().toString().equals(getString(R.string.referencia))){
                            referencia=referenciaEt.getText().toString();
                        }
                        if(!descripcionEt.getText().toString().equals("") && !descripcionEt.getText().toString().equals(getString(R.string.descripcion))){
                            descripcion=descripcionEt.getText().toString();
                        }

                        lanzarBusqueda(codigo, familia, referencia, descripcion);
                    }
                });
                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchDialog.dismiss();
                    }
                });
                searchDialog.show();
            }
        });

        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToQrActivity();
            }
        });

        duplicateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pedido pedido=new Pedido();
                pedido.setnPedido(numPedido);
                pedido.setDia(dia);
                pedido.setMes(mes);
                pedido.setAño(año);
                pedido.setPeso(peso);
                pedido.setImporte(importe);
                pedido.setAbierto(isAbierto);
                pedido.setArticulos(articulos);
                //Y aquí a ver qué hacemos con el pedido duplicado...si añadirlo al recyclerview o qué.
                ListActivity.datos.add(pedido);

            }
        });

        reloadButton=(ImageButton)findViewById(R.id.reloadbutton);
        reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.notifyDataSetChanged();
            }
        });

    }

    private void calcularImporteTotal() {
        Double impTotal=0D;
        for(int i=0;i<articulos.size();i++){
            impTotal+=articulos.get(i).getImporteTotal();
        }
        importeTotal.setText(String.valueOf(impTotal));
    }

    private void goToQrActivity() {
        Intent intent=new Intent(DetallePedidoActivity.this,QRActivity.class);
        startActivity(intent);
    }

    private void lanzarBusqueda(String codigo, String familia, String referencia, String descripcion) {
        Log.i("David", "Vamos a lanzar la búsqueda. Valores: ");
        Log.i("David", "Código: "+codigo);
        Log.i("David", "Familia: "+familia);
        Log.i("David", "Referencia: "+referencia);
        Log.i("David", "Descripción: "+descripcion);
        if(codigo.equals("") && familia.equals("") && referencia.equals("") && descripcion.equals("")){
            Toast.makeText(DetallePedidoActivity.this, getString(R.string.nosearchcriteria), Toast.LENGTH_LONG).show();
        }else{
            SearchBean bean=new SearchBean();
            bean.setCodigo(codigo);
            bean.setFamilia(familia);
            bean.setReferencia(referencia);
            bean.setDescripcion(descripcion);
            RestClient serviceLogin = ApiCliente.getService().create(RestClient.class);
            Call<String> searchResponse=serviceLogin.doSearch(bean);
            searchResponse.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if(response.isSuccessful()){
                        //La respuesta ha sido exitosa.
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {

                }
            });
        }


    }


}

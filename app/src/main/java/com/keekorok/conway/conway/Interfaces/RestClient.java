package com.keekorok.conway.conway.Interfaces;

import com.google.gson.JsonObject;
import com.keekorok.conway.conway.Beans.Articulo;
import com.keekorok.conway.conway.Beans.LoginResponse;
import com.keekorok.conway.conway.Beans.Pedido;
import com.keekorok.conway.conway.Beans.PedidosResponse;
import com.keekorok.conway.conway.Beans.SearchBean;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mac-27 on 5/4/17.
 */

public interface RestClient {

    @FormUrlEncoded
    @POST("Login")
    Call<String> login(@Field("usuario") String username, @Field("pass")String pass);

    @GET("urlPedidos")
    Call<Pedido> getPedidos();

    @GET("urlArticulos")
    Call<Articulo> getArticulosForNumPedido(@Query("numPedido") String numPedido);

    @POST("urlSearch")
    Call<String> doSearch(@Body SearchBean bean);
}
